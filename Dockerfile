FROM maven:3.8.3-openjdk-17 AS BUILD_IMAGE
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN mvn clean package -DskipTests

FROM eclipse-temurin:11
WORKDIR /root/
COPY --from=BUILD_IMAGE /project/target/studentsystem-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java","-jar","studentsystem-0.0.1-SNAPSHOT.jar"]
